package log

import (
	"encoding/json"
	"log"
	"testing"
)

func TestPrint(t *testing.T) {
	Print("TEST")
	log.Print("TEST")
	Println("TEST")
	log.Println("TEST")
}

func TestJSON(t *testing.T) {
	WriteError("TEST")

	out := make(map[string]interface{})
	out["test"] = "value"
	WriteError(out)
}

type testJsonLogger struct {
	t *testing.T
}

func (t testJsonLogger) Print(v ...interface{}) {
	if len(v) != 1 {
		t.t.Errorf("Wrong amount of arguments, %d", len(v))
		return
	}
	if out, ok := v[0].(Out); ok {
		b, err := json.Marshal(out)
		if err == nil {
			Print(string(b))
		} else {
			t.t.Error(err)
		}
	} else {
		t.t.Error("NOT OKAY")
	}
}

func TestInnerJSON(t *testing.T) {
	l := Logger{
		Level: Warning,
		Out:   testJsonLogger{t},
	}

	out := make(map[string]interface{})
	out["test"] = "value"
	l.WriteError(out)
}
