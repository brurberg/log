package log

import ()

func (l Logger) CheckBool(b bool, context, msg string, level Level) bool {
	if b {
		l.Write(Log{Level: level, Context: context, Message: msg})
		return true
	}
	return false
}

func CheckBool(b bool, context, msg string, level Level) bool {
	return LogHandler.CheckBool(b, context, msg, level)
}

func (l Logger) CheckError(context string, err error, level Level) bool {
	if err != nil {
		l.Write(Log{Level: level, Context: context, Message: err.Error()})
		return true
	}
	return false
}

func CheckError(context string, err error, level Level) bool {
	return LogHandler.CheckError(context, err, level)
}
