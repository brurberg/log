package log

import (
	"encoding/json"
	"log"
	"testing"
)

func TestPrint(t *testing.T) {
	Print("TEST")
	log.Print("TEST")
	Println("TEST")
	log.Println("TEST")
}

func TestJSON(t *testing.T) {
	Write(Log{Level: Error, Message: "TEST"})

	out := make(map[string]interface{})
	out["test"] = "value"
	Print(out)
}

type testJsonLogger struct {
	t *testing.T
}

func (t testJsonLogger) Print(e Log) {
	b, err := json.Marshal(e)
	if err == nil {
		Print(string(b))
	} else {
		t.t.Error(err)
	}
}

func TestInnerJSON(t *testing.T) {
	l := Logger{
		Level: Info,
		Out:   testJsonLogger{t},
	}

	out := make(map[string]interface{})
	out["test"] = "value"
	l.Print(out)
}
