package log

import ()

type Level int

const (
	Clean Level = iota
	Debug
	Info
	Success
	Warning
	Error
	Critical
)

func LevelToString(level Level) string {
	switch level {
	case Debug:
		return "Debug"
	case Info:
		return "Info"
	case Success:
		return "Success"
	case Warning:
		return "Warning"
	case Error:
		return "Error"
	case Critical:
		return "Critical"
	default:
		return ""
	}
}
