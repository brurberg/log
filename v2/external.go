package log

import (
	"io"
	"log"
	"net/http"
)

type ExternLogger struct {
	Endpoint     string
	LogToPackage func(Log) (io.Reader, error)
}

func (l ExternLogger) Print(e Log) {
	go func() {
		buf, err := l.LogToPackage(e)
		if err != nil {
			log.Println(err)
			return
		}
		req, _ := http.NewRequest("POST", l.Endpoint, buf)
		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		if _, err := client.Do(req); err != nil {
			log.Println(err)
			return
		}
	}()
}
