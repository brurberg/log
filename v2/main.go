package log

import (
	"fmt"
	"log"
	"os"
)

type Log struct {
	Level   Level
	Context string
	Message string
}

var LogHandler = Logger{Level: Info, Out: LocalLogger{}}

type printer interface {
	Print(Log)
}

type Logger struct {
	Level Level
	Out   printer
}

type LocalLogger struct{}

func (l LocalLogger) Print(e Log) {
	out := log.New(os.Stdout, "", log.LstdFlags)
	if e.Context != "" {
		out.Print(fmt.Sprintf("[%s]%s: %s", LevelToString(e.Level), e.Context, e.Message))
	} else {
		out.Print(fmt.Sprintf("%s: %s", LevelToString(e.Level), e.Message))
	}
}

func (l Logger) Write(e Log) {
	if l.Level > e.Level {
		return
	}
	l.Out.Print(e)
}

func Write(e Log) {
	LogHandler.Write(e)
}

func (l Logger) Print(v ...interface{}) {
	l.Write(Log{Level: Info, Message: fmt.Sprint(v...)})
}

func Print(v ...interface{}) {
	LogHandler.Print(v...)
}

func (l Logger) Printf(format string, v ...interface{}) {
	l.Write(Log{Level: Info, Message: fmt.Sprintf(format, v...)})
}

func Printf(format string, v ...interface{}) {
	LogHandler.Printf(format, v...)
}

func (l Logger) Println(v ...interface{}) {
	l.Print(v...)
}

func Println(v ...interface{}) {
	LogHandler.Println(v...)
}

func (l Logger) Fatal(v ...interface{}) {
	l.Write(Log{Level: Critical, Message: fmt.Sprint(v...)})
	os.Exit(1)
}

func Fatal(v ...interface{}) {
	LogHandler.Fatal(v...)
}
