package log

import (
	"fmt"
	"log"
	"os"
)

var LogHandler = LocalLogger()

const (
	Clean int = iota
	Debug
	Info
	Success
	Warning
	Error
)

type printer interface {
	Print(...interface{})
}

type Logger struct {
	Level int
	Out   printer
}

type Out struct {
	Level   int    `json:"level"`
	Message string `json:"message"`
}

func LevelToString(level int) string {
	switch level {
	case Debug:
		return "Debug"
	case Info:
		return "Info"
	case Success:
		return "Success"
	case Warning:
		return "Warning"
	case Error:
		return "Error"
	default:
		return ""
	}
}

func LocalLogger() Logger {
	return Logger{
		Level: Warning,
		Out:   log.New(os.Stdout, "", log.LstdFlags),
	}
}

func (l Logger) Write(level int, message ...interface{}) {
	if l.Level > level {
		return
	}
	l.Out.Print(Out{level, fmt.Sprint(message...)})
}

func (l Logger) WriteDebug(message ...interface{}) {
	l.Write(Debug, message...)
}

func WriteDebug(v ...interface{}) {
	LogHandler.WriteDebug(v...)
}

func (l Logger) WriteInfo(message ...interface{}) {
	l.Write(Info, message...)
}

func WriteInfo(v ...interface{}) {
	LogHandler.WriteInfo(v...)
}

func (l Logger) WriteSuccess(message ...interface{}) {
	l.Write(Success, message...)
}

func WriteSuccess(v ...interface{}) {
	LogHandler.WriteSuccess(v...)
}

func (l Logger) WriteWarning(message ...interface{}) {
	l.Write(Warning, message...)
}

func WriteWarning(v ...interface{}) {
	LogHandler.WriteWarning(v...)
}

func (l Logger) WriteError(message ...interface{}) {
	l.Write(Error, message...)
}

func WriteError(v ...interface{}) {
	LogHandler.WriteError(v...)
}

func (l Logger) CheckError(err error, level int) bool {
	if err != nil {
		l.Write(level, err)
		return true
	}
	return false
}

func CheckError(err error, level int) bool {
	return LogHandler.CheckError(err, level)
}

func (l Logger) Print(v ...interface{}) {
	l.Out.Print(v...)
}

func Print(v ...interface{}) {
	LogHandler.Print(v...)
}

func (l Logger) Println(v ...interface{}) {
	l.Out.Print(v...)
}

func Println(v ...interface{}) {
	LogHandler.Println(v...)
}

func Fatal(v ...interface{}) {
	LogHandler.WriteError(v...)
	os.Exit(1)
}
